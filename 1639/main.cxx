#include "main.hxx"

int main(const int argc, const char *argv[])
{
  std::string mNumber, nNumber;
  int mBack, nBack;
  std::cin >> mNumber >> nNumber;
  mBack = mNumber.back() - '0';
  nBack = nNumber.back() - '0';
  if ( (mBack * nBack) % 2 )
    {
      std::cout << "[second]=:]";
    }
  else
    {
      std::cout << "[:=[first]";
    }
  
  return EXIT_SUCCESS;
}
