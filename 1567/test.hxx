#ifndef TEST_HXX
#define TEST_HXX

#include <functional>
#include <algorithm>
#include <exception>
#include <iostream>
#include <typeinfo>
#include <fstream>
#include <sstream>
#include <cassert>
#include <string>
#include <chrono>
#include <array>
#include <list>

void test();
template<typename T>
void success(const T);
void failure();
void info(const std::string);
std::string ssystem(const std::string, std::list<std::string>);
std::string ltrim(std::string);
std::string rtrim(std::string);
std::string trim(std::string);

#endif
