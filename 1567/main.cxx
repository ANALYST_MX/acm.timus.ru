#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int i, cost = 0;
  std::string sentence;
  std::getline(std::cin, sentence);
  std::array<std::string, 11> keys = {
    "abc",
    "def",
    "ghi",
    "jkl",
    "mno",
    "pqr",
    "stu",
    "vwx",
    "yz",
    ".,!",
    " "
  };

  for ( auto c : sentence )
    {
      for ( auto k : keys )
	{
	  if ( (i = k.find(c)) != std::string::npos )
	    {
	      cost += i + 1;
	    }
	}
    }

  std::cout << cost;

  return EXIT_SUCCESS;
}
