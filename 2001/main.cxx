#include "main.hxx"

int main(const int argc, const char *argv[])
{
  const int COUNT = 3;
  int a[COUNT], b[COUNT];
  for ( unsigned i = 0; i < COUNT; ++i )
    {
      std::cin >> a[i] >> b[i];
    }
  std::cout << (b[2] - b[0]) << ' ' << (a[1] - a[0]) << std::endl;

  return EXIT_SUCCESS;
}
