#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int n, position;
  std::cin >> n;
  int pixels[n][n];

  for ( auto x = 0; x < n; ++x )
    {
      for ( auto y = 0; y < n; ++y )
	{
	  std::cin >> position;
	  pixels[x][y] = position;
	}
    }

  for ( auto i = 0; i < n; ++i)
    {
      for ( auto x = 0; x <= i; ++x )
	{
	  std::cout << pixels[i - x][x] << ' ';
	}
    }

  for ( auto i = 1; i < n; ++i)
    {
      for ( auto x = i; x < n; ++x )
	{
	  std::cout << pixels[i + n - 1 - x][x] << ' ';
	}
    }
  
  return EXIT_SUCCESS;
}
