#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int n, a, b, weight;
  std::cin >> n >> a >> b;

  weight = 2 * ((a * b) * n);
  std::cout << weight << std::endl;
  return EXIT_SUCCESS;
}
