#include "main.hxx"

int main(const int argc, const char *argv[])
{
  std::array<int, 8> numeration = {0,1,2,3,4,5,6,7};
  std::list<int> results;
  int xy[8][2] = {{2, 1},{2, -1},{1, -2},{-1, -2},{-2, -1},{-2, 1},{-1, 2},{1, 2}};
  char x;
  int n, y;
  std::cin >> n;
  for ( auto i = 0; i < n; ++i )
    {
      std::cin >> x >> y;
      int count = 0;
      for ( auto j = 0; j < 8; ++j )
	{
	  if ( (x - 97 + xy[j][0] >= 0) && (x - 97 + xy[j][0] <= 7) && (y - 1 + xy[j][1] >= 0) && (y - 1 + xy[j][1] <= 7) )
	    {
	      ++count;
	    }
	}
      results.push_back(count);
    }
  for ( auto it = results.cbegin(); it != results.cend(); ++it )
    {
      std::cout << *it << std::endl;
    }
  
  return EXIT_SUCCESS;
}
