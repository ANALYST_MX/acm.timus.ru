#include "main.hxx"

int main(const int argc, const char *argv[])
{
  std::queue<bool> fifo;
  int k, n, count;
  std::cin >> k >> n;
  for ( auto i = 0; i < n; ++i )
    {
      std::cin >> count;
      for ( auto j = 0; j < count; ++j )
	{
	  fifo.push(true);
	}
      for ( auto j = fmin(k, fifo.size()); j > 0; --j )
	{
	  fifo.pop();
	}
    }

  std::cout << fifo.size() << std::endl;
  
  return EXIT_SUCCESS;
}
