#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int n, count = 1;
  long int val, last;
  std::vector<long int> numbers;
  std::cin >> n;
  std::cin >> last;
  for ( auto i = 1; i < n; ++i )
    {
      std::cin >> val;
      if ( last == val )
	{
	  ++count;
	}
      else
	{
	  numbers.push_back(count);
	  numbers.push_back(last);
	  last = val;
	  count = 1;
	}
    }
  numbers.push_back(count);
  numbers.push_back(last);

  std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<long int>(std::cout, " "));
  
  return EXIT_SUCCESS;
}
