#include "main.hxx"

int main(const int argc, const char *argv[])
{
  const int MAX_SIZE = 3;
  long int n, position = 0, max = 0, current = 0, power;
  std::list<long int> sections;
  std::cin >> n;
  for ( auto i = 0; i < n; ++i )
    {
      std::cin >> power;
      sections.push_back(power);
      if ( i == MAX_SIZE - 1 )
	{
	  max = std::accumulate(sections.begin(), sections.end(), 0L);
	  position = i;
	}
      else if ( i >= MAX_SIZE )
	{
	  sections.pop_front();
	  current = std::accumulate(sections.begin(), sections.end(), 0L);
	  if ( current > max )
	    {
	      max = current;
	      position = i;
	    }
	}
    }

  std::cout << max << ' ' << position;
  
  return EXIT_SUCCESS;
}
