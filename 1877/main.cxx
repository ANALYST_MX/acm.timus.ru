#include "main.hxx"

int main(const int argc, const char *argv[])
{
  const char *YES = "yes";
  const char *NO = "no";
  int key1, key2;
  std::cin >> key1 >> key2;
  if ( key1 % 2 == 0 || key2 % 2 == 1 )
    {
      std::cout << YES << std::endl;
    }
  else
    {
      std::cout << NO << std::endl;
    }

  return EXIT_SUCCESS;
}
