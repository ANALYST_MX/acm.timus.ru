#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int v = 0, n;
  std::cin >> n;
  int arr[n * n];

  for ( auto i = 1; i <= n; ++i)
    {
      for ( auto x = 0, y = n - i; y < n; ++x, ++y )
	{
	  arr[x * n + y] = ++v;
	}
    }

  for ( auto i = 1; i < n; ++i)
    {
      for ( auto x = i, y = 0; x < n; ++x, ++y )
	{
	  arr[x * n + y] = ++v;
	}
    }

  for ( auto i = 1; i <= n * n; ++i )
    {
      std::cout << arr[i - 1];
      if ( i % n == 0 )
	{
	  std::cout << std::endl;
	}
      else
	{
	  std::cout << ' ';
	}
    }
  
  return EXIT_SUCCESS;
}
