#include "main.hxx"

int main(const int argc, const char *argv[])
{

  int num, total;
  std::cin >> num;
  total = abs(num) * (abs(num) + 1) / 2;
  if ( num < 0 )
    {
      total = -1 * total + 1;
    }
  else if ( num == 0 )
    {
      total = 1;
    }

  std::cout << total << std::endl;
  
  return EXIT_SUCCESS;
}
