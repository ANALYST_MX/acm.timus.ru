#ifndef TEST_HXX
#define TEST_HXX

#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <string>
#include <array>
#include <list>

void test();
void success();
void failure();
void info(const std::string);
std::string ssystem(const std::string, std::list<std::string>);

#endif
