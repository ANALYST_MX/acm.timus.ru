#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int num;
  std::string anindilyakwa;
  std::cin >> num;
  
  switch (num)
    {
    case 1 ... 4:
      anindilyakwa = "few";
      break;
    case 5 ... 9:
      anindilyakwa = "several";
      break;
    case 10 ... 19:
      anindilyakwa = "pack";
      break;
    case 20 ... 49:
      anindilyakwa = "lots";
      break;
    case 50 ... 99:
      anindilyakwa = "horde";
      break;
    case 100 ... 249:
      anindilyakwa = "throng";
      break;
    case 250 ... 499:
      anindilyakwa = "swarm";
      break;
    case 500 ... 999:
      anindilyakwa = "zounds";
      break;
    case 1000 ... 2000:
      anindilyakwa = "legion";
      break;
  }

  std::cout << anindilyakwa << std::endl;
  return EXIT_SUCCESS;
}
