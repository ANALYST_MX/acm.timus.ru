#include "main.hxx"

int main(const int argc, const char *argv[])
{
  double intpart;
  int n;
  long long int k, v;
  std::cin >> n;
  std::list<char> l;
  for ( auto i = 0; i < n; ++i )
    {
      std::cin >> k;
      if ( modf(((sqrt(1 + 8 * (k - 1)) - 1) / 2), &intpart) == 0 )
	{
	  l.push_back('1');
	}
      else
	{
	  l.push_back('0');
	}
    }

  for ( auto it = l.begin(); it != l.end(); ++it )
    {
      std::cout << *it << ' ';
    }
  
  return EXIT_SUCCESS;
}
