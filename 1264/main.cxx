#include "main.hxx"

int main(const int argc, const char *argv[])
{
  long long int n, m;
  std::cin >> n >> m;
  std::cout << n * ++m << std::endl;

  return EXIT_SUCCESS;
}
