#include "main.hxx"

int main(const int argc, const char *argv[])
{
  std::forward_list<long long int> numbers;
  std::copy(std::istream_iterator<long long int>(std::cin), std::istream_iterator<long long int>(), front_inserter(numbers));
  for_each(numbers.begin(), numbers.end(), printSqrt);
  
  return EXIT_SUCCESS;
}

void printSqrt(long long int &num)
{
  std::cout.precision(4);
  std::cout << std::fixed << sqrt(static_cast<long double>(num)) << std::endl;
}
