#include "main.hxx"

int main(const int argc, const char *argv[])
{
  const double PI = 3.14159265358979323846;
  double s;
  int l, r;
  std::cin >> l >> r;
  if ( (r + r) <= l )
    {
      s = PI * r * r;
    }
  else if ( (r + r) < sqrt(2 * l * l) )
    {
      double h = r - static_cast<double>(l) / 2;
      s = PI * r * r - 4 * (r * r * acos(1. - h / r) - (r - h) * sqrt(2 * h * r - h * h));
    }
  else
    {
      s = l * l;
    }
  std::cout.precision(3);
  std::cout << std::fixed << s;
  
  return EXIT_SUCCESS;
}
