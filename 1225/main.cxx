#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int n;
  long long int num1 = 0, num2 = 1, numNext = num1 + num2;
  std::cin >> n;
  for (int i = 0; i < n-2; i++){
    num1 = num2;
    num2 = numNext;
    numNext = num1 + num2;
  }
  std::cout << 2 * numNext;
  
  return EXIT_SUCCESS;
}
