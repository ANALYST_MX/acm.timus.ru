#include "test.hxx"

int main(const int argc, const char *argv[])
{
  test();
  
  return EXIT_SUCCESS;
}

void test()
{
  std::cout << "\t\033[1;34mstart test\033[0m" << std::endl;
  info("result");
  try
    {
      std::list<std::string> args;
      args.push_back("4 7");
      assert(ssystem("./main", args) == "6 3");
      success();
    }
  catch (std::exception &e)
    {
      std::cerr << e.what() << std::endl;
      failure();
    }
  std::cout << "\t\033[1;34mend test\033[0m" << std::endl;
}

std::string ssystem(const std::string command, std::list<std::string> args)
{
  std::array<char, sizeof(P_tmpdir "/fileXXXXXX")> tmpin = { P_tmpdir "/fileXXXXXX" };
  std::array<char, sizeof(P_tmpdir "/fileXXXXXX")> tmpout = { P_tmpdir "/fileXXXXXX" };
  mkstemp64(tmpin.data());
  mkstemp64(tmpout.data());
  std::ofstream fin(tmpin.data(), std::ofstream::out);
  if ( fin )
    {
      for ( auto it = args.begin(); it != args.end(); ++it )
	{
	  fin << *it << std::endl;
	}
      fin.close();
    }
  std::string cmd = command + " < " + tmpin.data() + " > " + tmpout.data();
  std::system(cmd.c_str());
  std::ifstream fout(tmpout.data(), std::ios::in);
  std::ostringstream sout;
  copy(std::istreambuf_iterator<char>(fout),
       std::istreambuf_iterator<char>(),
       std::ostreambuf_iterator<char>(sout));
  std::string result = sout.str();
  if ( fout )
    {
      fout.close();
    }

  remove(tmpin.data());
  remove(tmpout.data());
  return result.substr(0, result.length() - 1);
}

void success()
{
  std::cout << "\033[1;32msuccess\033[0m" << std::endl;
}

void failure()
{
  std::cout << "\033[1;31mfailure\033[0m" << std::endl;
}

void info(const std::string msg)
{
  std::cout << msg << ": ";
}
