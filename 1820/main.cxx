#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int order, steaks, minutes;
  std::cin >> order >> steaks;
  if ( order > steaks )
    {
      minutes = ceil(2. * order / steaks);
    }
  else
    {
      minutes = 2;
    }
  
  std::cout << minutes << std::endl;

  return EXIT_SUCCESS;
}
