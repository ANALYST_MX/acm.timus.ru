#include "main.hxx"

int main(const int argc, const char *argv[])
{
  const int TIMES = 3;
  int count = 0, n;
  unsigned long int number;
  std::map<unsigned long int, int> stat;
  for ( auto i = 0; i < TIMES; ++i )
    {
      std::cin >> n;
      for ( auto j = 0; j < n; ++j )
	{
	  std::cin >> number;
	  stat[number] += 1;
	}
    }

  for ( auto it = stat.begin(); it != stat.end(); ++it )
    {
      if ( it->second == 3 )
	{
	  ++count;
	}
    }
  std::cout << count;
  
  return EXIT_SUCCESS;
}
