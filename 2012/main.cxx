#include "main.hxx"

int main(const int argc, const char *argv[])
{
  int f;
  const int MINUTES_IN_HOUR = 60;
  const int ALL_TIME = 5 * MINUTES_IN_HOUR;
  const int TASK = 12;
  const int INTERVAL = 45;

  std::cin >> f;
  if ( (ALL_TIME - 1 * MINUTES_IN_HOUR) >= (12 - f) * INTERVAL )
    {
      std::cout << "YES";
    }
  else
    {
      std::cout << "NO";
    }
  
  return EXIT_SUCCESS;
}
